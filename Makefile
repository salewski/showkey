# showkey -- keystroke echoer
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: MIT

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

showkey: showkey.c
	$(CC) -DREVISION=$(VERS) showkey.c -o showkey

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f showkey showkey.o showkey.1 *.html *.tar.gz
	rm -f MANIFEST

cppcheck:
	cppcheck --quiet -DREVISION=$(VERS) showkey.c

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

install: showkey.1
	cp showkey /usr/bin/showkey
	cp showkey.1 /usr/share/man/man1

uninstall:
	rm /usr/bin/showkey
	rm /usr/share/man/man1/showkey.1

SOURCES = README COPYING NEWS control Makefile showkey.c showkey.adoc

showkey-$(VERS).tar.gz: $(SOURCES) showkey.1
	@ls $(SOURCES) showkey.1 | sed s:^:showkey-$(VERS)/: >MANIFEST
	@(cd ..; ln -s showkey showkey-$(VERS))
	(cd ..; tar -czf showkey/showkey-$(VERS).tar.gz `cat showkey/MANIFEST`)
	@(cd ..; rm showkey-$(VERS))

version:
	@echo $(VERS)

dist: showkey-$(VERS).tar.gz

release: showkey-$(VERS).tar.gz showkey.html
	shipper version=$(VERS) | sh -e -x

refresh: showkey.html
	shipper -N -w version=$(VERS) | sh -e -x
