= showkey(1) =
:doctype: manpage

== NAME ==
showkey - echo raw keystrokes back at you in a printable form

== SYNOPSIS ==
*showkey*

[[description]]
== DESCRIPTION ==

This program puts your terminal in raw mode, eats keystrokes,
and prints them back it you in a recognizable printed form. It will be
useful if you're not certain what various keyboard keys are
sending.

Non-printables (and whitespace) ASCII characters are displayed as cookies
surrounded by < >. The cookie will always contain an ASCII
mnemonic (NUL, SOH, STX, ETX, EOT, ENQ, ACK, BEL, BS, HT, LF, VT, FF,
CR, SO, SI, DLE, DC1, DC2, DC3, DC4, NAK, SYN, ETB, CAN, EM, SUB, ESC,
FS, GS, RS, US, SP) and may also contain a control-key formula.

Characters with the high bit set are shown as <ALT-xxx>,
where xxx is generated as previously described.

You terminate the program with your shell interrupt or quit
characters.

[[bugs]]
== BUGS ==
Under some (but not all) X window managers you may see the
following message: "Couldn't get a file descriptor referring to the
console".  If this happens, go root and try again.

[[author]]
== AUTHOR ==
Eric S. Raymond <esr@thyrsus.com>

// end
